// Selectors - getElementById

let company = document.getElementById('company');

console.log(company);

company.textContent = 'SkillVersum';

company.innerHTML = 'My Company';

console.dir(company);



//Selectors - getElementsByClassName

let headers = document.getElementsByClassName('card-header');

console.log(headers);

/*for (let i = 0; i < headers.length; i++) {
	headers[i].style.backgroundColor = 'darkgray';
}
*/

for(let header of headers){
	header.style.backgroundColor = 'darkgray';
}


// Selectors - getElementsByTagName

let anchors = document.getElementsByTagName('a');

console.log(anchors);

for(let a of anchors){
	a.style.fontWeight = 'bold';
}

//Selectors QuerySelector.


var leadDiv = document.querySelector('.mx-auto');

console.log(leadDiv);


leadDiv.style.border = '2px solid lightgray';


var navAnchor = document.querySelector('a');
console.log(navAnchor);

navAnchor.style.fontSize = 'xx-large';


var h5 = document.querySelector('#company');

console.log(h5);

h5.style.color = "crimson";

//Selectors querySelectorAll.

var divs = document.querySelectorAll('.card-body');

console.log(divs);


for(var div of divs){
	div.style.backgroundColor = 'lightblue'
}


